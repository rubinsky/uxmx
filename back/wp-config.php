<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'restwp' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'g9NZ~O#DMLhD&9q+MpoTG{JP,nP)Yw_P=}[R~8_ME]k{~7c+!WQW5*~2u<xdCKb|' );
define( 'SECURE_AUTH_KEY',  ' 7k70hppHjJ[aM0)+FD?+b!)QK(S5i,^MUS2yVJ<eyh} [x7ske>@pG:,$Z9Q.uX' );
define( 'LOGGED_IN_KEY',    '{x*p8x:%ujMaUS@2hRtw.yE-HG0BBvb-YdOGx3r~!f<OL]tCK<,@Q+}}5;4iPDs7' );
define( 'NONCE_KEY',        ']m6RAokv:~QoZ>u]SwJv/*N!|bs|g.~vggDg[#+iSw5&rp,}kZa0RkIN2JN)sD}g' );
define( 'AUTH_SALT',        'Zj$QQ!#TpGl{v_Sho_%v2#I =Kt4_d.E%:Vo28MN}E5qDsTlPw4}S+GI9hz/N6g9' );
define( 'SECURE_AUTH_SALT', 'E+$y8da:M^r}U6itt.C=8L&kL>xu4x+~j 1);N(I_yO7e {A2E6s7fOi-Fm[wS[e' );
define( 'LOGGED_IN_SALT',   'm=KBo]dhO<XpU`T$7K_I3Y|fIa$feb3qxNYmOy;f(;ZCHl>`[4q<17]([A>/$J-Y' );
define( 'NONCE_SALT',       ']W<fyUKC?xlvn`33s,<e?zrqc8UQ;ixFJI)MVd}~/YfM*7{%-FSk^WTldEi1h[X&' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'rwp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
